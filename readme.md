- Install Node.js v12.21.0 or above.
- Enter `npm install` in the root directory.
- Run `npm run start` to start the server process.