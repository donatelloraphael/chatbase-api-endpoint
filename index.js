const consola = require("consola");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const axios = require("axios");
const axiosRetry = require("axios-retry");
require("dotenv").config();

const corsOptions = {
  origin: process.env.CORS_ORIGIN,
  methods: "GET,POST, PUT, PATCH, DELETE",
  preflightContinue: false,
  optionsSuccessStatus: 200,
};

const app = express();

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

axiosRetry(axios, { 
	retries: 10,
	shouldResetTimeout: true,
	retryCondition: (_error) => true // retry no matter what error
});

const headers = {
	"Content-Type": "application/json",
	"cache-control": "no-cache" 
};

(function startServer() {

	// Start API
	app.listen(process.env.API_PORT, process.env.API_HOST);

	consola.ready({
    message: `API server listening on http://${process.env.API_HOST}:${process.env.API_PORT}`,
    badge: true
  });

  app.post(process.env.API_URI, async (req, res) => {

  	const user = {
  		api_key: req.body.api_key || process.env.DEFAULT_API_KEY,
  		type: req.body.type || "user",
  		platform: req.body.platform || "Robo",
	    message: req.body.message || "Test",
	    intent: req.body.intent || "greeting",
	    version: req.body.version || process.env.VERSION,
	    user_id:  req.body.user_id ||process.env.USER_ID || "user-00",
	    // not_handled: true,
    	// url: "https://chatbase.com/",
  	}

  	const data = await axios.post(process.env.CHATBASE_API_URI, user, { headers: headers, timeout: 3000 })
  										.then(res => res.data)
  										.catch(err => {
  											console.log(error);
  											return error.response.data;
  										});

  	res.send(data);

  });

})();